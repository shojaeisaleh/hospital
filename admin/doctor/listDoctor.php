

<?php 

    $result = show_doctor();
    
?>
<div class="col-lg-12">
<section class="panel">
<?php 
if(isset($_SESSION['doctor']) && $_SESSION['doctor'] == "update_doctor_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> بروزرسانی با موفقیت انجام شد.
    </div>
    ';
    unset($_SESSION['doctor']);
}


if(isset($_SESSION['doctor']) &&$_SESSION['doctor'] == "add_doctor_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> پزشک با موفقیت اضافه شد.
    </div>
    ';
    unset($_SESSION['doctor']);
}

if(isset($_SESSION['doctor']) && $_SESSION['doctor'] == "delete_doctor_ok"){
    echo '
    <div class="alert alert-block alert-success fade in large-fontsize">
        <strong>تبریک :</strong> پزشک با موفقیت حذف شد.
    </div>
    ';
    unset($_SESSION['doctor']);
}
    if(mysqli_num_rows($result) > 0):
?>
    <header class="panel-heading">
        لیست پزشک های سایت
    
    </header>
    <div class="row">
        <?php 
            while($row = mysqli_fetch_assoc($result)):
        ?>
    <div class="col-lg-4 m">
            <!--widget start-->
            <aside class="profile-nav alt green-border">
                <section class="panel">
                    <div class="user-heading alt" style="background:rgba(<?php echo rand(100,150); ?>,<?php echo rand(50,100); ?>,<?php echo rand(100,255); ?>,1);">
                        <a href="#">
                        
                            <img alt="" src="<?php echo $row['image']; ?>">
                        </a>
                        <h1><?php echo $row['name']." ".$row['lastname'] ?></h1>
                        <p>بیمارستان: <?php echo implode(" ",show_hospital_by_id($row['hospital_id'])); ?></p>
                        <p>تخصص: <?php echo $row['expertise']; ?></p>
                        <a href="dashboard.php?m=doctor&p=updateDoctor&id=<?php echo $row['id']; ?>" style="border:none"><button type="button" class="btn btn-round btn-success">ویرایش</button></a>
                        <a href="dashboard.php?m=doctor&p=deleteDoctor&id=<?php echo $row['id']; ?>" style="border:none"><button type="button" class="btn btn-round btn-danger">حذف</button></a>
                    </div>

                   

                </section>
            </aside>
            <!--widget end-->
            <!--widget start-->
            <div class="panel">
                <div class="panel-body">
                
                </div>
            </div>
            <!--widget end-->
        </div>
    
            <?php endwhile; ?>
    
    
    
    
    
    </div>
<?php
else: 
    echo '    
    <div class="alert alert-block alert-info fade in large-fontsize">
    <strong>راهنمایی :</strong> ابتدا از بخش مدیریت پزشک ها یک پزشک اضافه کنید. 
    </div>';
    endif;
?>
</section>
</div>