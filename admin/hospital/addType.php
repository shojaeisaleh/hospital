<?php 
    if (isset($_POST['btn'])){
        $name = $_POST['name'];
        add_parent_hospital($name);
    }
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
              ایجاد دسته برای بیمارستان
            
            </header>
            <div class="panel-body">
                <form role="form" method="post">
                    <div class="form-group">
                        <label for="name">نوع بیمارستان را وارد کنید:</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder=" نوع بیمارستان را وارد کنید">
                    </div>

                    <button type="submit" class="btn btn-info" name="btn">اضافه کردن دسته جدید</button>
                </form>

            </div>
        </section>
    </div>
</div>