<?php
 include_once 'functions/patient.php';

?>
<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta name="keywords" content="بیمارستان شریعتی, بیمارستان شریعتی تهران, نوبت دهی شریعتی, نوبت دهی اینترنتی شریعتی,نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی">
    <title>نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی</title>
    
    <link href="assets/css/css.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">

     <link href="assets/fonts/css/all.css" rel="stylesheet">

</head>
<body>
    <header>
        <div class="container header0">
            <div class="row">
                <div class="col col-lg-11 text-right">
                    
                    
                    
                </div>
                <div class="col col-lg-1 text-left">
                    <img src="assets/css/logo.png">
                </div>
            </div>
        </div>
        <!-- <div class="container-fluid header_gradient">
         </div> -->
    </header>
     <section id="body">
        <div class="container-fluid all-page ">
            <div class="container-fluid main-body-top" style="min-height: 426px;">
                <div class="container-fluid header1">
                    <div class="container header2">
                        <div class="row">
                            <div class="col col-lg-8">
                                
                                <div class="col col-lg-12 text-right">
                                    <h4 class="title_">درمانگاه های تخصصی و فوق تخصصی بیمارستان دکتر شریعتی</h4>
                                </div>
                            </div>
                            <div class="col col-lg-4 text-left">
                                <a href="index.php" style="margin-top: 18px" class="btn btn-white ">صفحه  نخست</a>
                                <a href="#" style="margin-top: 18px" class="btn btn-white back">صفحه قبل</a>
                            </div>
                        </div>
                    </div>
                </div>
                  



                <ul class="breadcrumb">

    

<li class="active">
    <a href="#">انتخاب نوع درمانگاه  </a> 
</li>        
<?php if(isset($_GET['type'])): ?>                              
<li class="active">
    <a href="#">انتخاب بیمارستان  </a> 
</li>  
<?php  endif; if(isset($_GET['child'])): ?>
<li class="active">
    <a href="#">انتخاب پزشک  </a> 
</li>  
<?php endif; ?>
</ul>


<div class="container-fluid main-body_" style="min-height: 426px;">
    <div class="container content text-center">

        <div class="seprator">
            <span class="glyphicon glyphicon-star sep-glyp1"></span>
            <span class="glyphicon glyphicon-star sep-glyp2"></span>
            <span class="glyphicon glyphicon-star sep-glyp3"></span>
            <h3>لطفا یک پزشک انتخاب کنید.</h3>
            <div class="course-div-sep1">
                <div class="course-div-sep2"></div>
            </div>
        </div>
        <div class="row">


        <?php
            if(isset($_GET['type']) && isset($_GET['child'])):
                $result = show_doctor_by_hospital_id($_GET['child']);
            
                while($row = mysqli_fetch_assoc($result)):
                    // var_dump($row);  
                
                $res = show_hospital_name_by_id($row['hospital_id']);
                $data = mysqli_fetch_assoc($res);       
                   
        ?>

        <div class="col-lg-6">
                <div class="panel program">
                    <div class="panel-body">
                     <div class="panel-img">
                       <img src="<?php if(basename($row['image']) == "doctor-default.png"){ echo "assets/img/".basename($row['image']);} else{
                          echo "admin/uploads/".basename($row['image']);
                       } ?>" class="img-thumbnail" title="">
                     </div>
                       <h4 class="panel-h">عنوان درمانگاه :  <span><?php echo implode(" ",$data);  ?></span></h4>
                       <h4 class="panel-h">نوع تخصص : <span> <?php echo $row['expertise']; ?> </span></h4>
                         <a href="date.php?type=<?php echo $_GET['type']; ?>&child=<?php echo $_GET['child']; ?>&doctor_id=<?php echo $row['id']; ?>">
                         <button style="margin:40px auto" type="button"  class="btn btn-info btn1 tmpClass">
                            جهت رزرو کلیک کنید
                        </button>  
                         </a>
                    </div>
                </div>
        </div>

        <?php 
                endwhile;
            endif;
        ?>


    </div>
</div>



            </div>
            <div class="container-fluid footer">
                <div class="container">
                    <h4 class="company">
                        
                    </h4>
                    <p>ساخته شده توسط : سحر تیموری</p>
                </div>
            </div>
        </div>

    </section>
    <!-- <script src="assets/js/jquery.js"></script> -->

    <!-- <script src="assets/js/bootstrap.js"></script> -->

    
    <script>
        var WindowHeight = $(document).height() - 1;
        var HeaderHeight = $('.header0').innerHeight() + $('.header_gradient').innerHeight() + $('.header1').innerHeight();
        var footerHeight = $('.footer').innerHeight();
        var bodyHeight = WindowHeight - HeaderHeight - footerHeight;
        $('.main-body_').css('min-height', bodyHeight);
        $('.main-body-top').css('min-height', bodyHeight);
        $('.back').click(function () {
            history.back();
            return false;
           // history.go(-1)
        })
    </script>



</body></html>