<?php 
    include_once 'header.php';
?>
    <!-- END header -->
<?php 
    include_once 'slider.php';
?>
    <!-- END slider -->
    <section class="container home-feature mb-5">
        <div class="row">
            <div class="col-md-4 p-0 one-col element-animate fadeInUp element-animated">
                <div class="col-inner p-xl-5 p-lg-5 p-md-4 p-sm-4 p-4">
                    <span class="icon flaticon-hospital-bed"></span>
                    <a href="cancel.php">
                        <h3>لغو نوبت</h3>
                    </a>
                </div>
                <a href="cancel.php" class="btn-more"></a>
            </div>
           
                <div class="col-md-4 p-0 two-col element-animate fadeInUp element-animated">
                    <a href="add.php">
                        <div class="col-inner p-xl-5 p-lg-5 p-md-4 p-sm-4 p-4">
                            <span class="icon flaticon-first-aid-kit"></span>

                            <h3>شروع نوبت دهی</h3>

                        </div>
                        </a><a href="add.php" class="btn-more"><h3></h3></a>
                    
</div>
           
            <div class="col-md-4 p-0 three-col element-animate fadeInUp element-animated">
                <div class="col-inner p-xl-5 p-lg-5 p-md-4 p-sm-4 p-4">
                    <span class="icon flaticon-hospital"></span>

                    <a href="list.php">
                        <h3>پیگیری نوبت</h3>
                    </a>
                </div>
                <a href="list.php" class="btn-more"></a>
            </div>
        </div>
    </section>

<?php 
    include_once 'footer.php';
?>