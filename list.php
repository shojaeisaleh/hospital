<?php
include_once 'functions/patient.php';

if(isset($_POST['btn'])){
    $data = $_POST['code'];
    if($data !== ""){
        $result = show_patient_by_code($data);
        
    }
}

?>
<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta name="keywords" content="بیمارستان شریعتی, بیمارستان شریعتی تهران, نوبت دهی شریعتی, نوبت دهی اینترنتی شریعتی,نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی">
    <title>نوبت دهی اینترنتی درمانگاه های بیمارستان شریعتی</title>
    
    <link href="assets/css/css.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">

     <link href="assets/fonts/css/all.css" rel="stylesheet">

</head>
<body>
    <header>
        <div class="container header0">
            <div class="row">
                <div class="col col-lg-11 text-right">
                    
                    
                    
                </div>
                <div class="col col-lg-1 text-left">
                    <img src="assets/css/logo.png">
                </div>
            </div>
        </div>
        <!-- <div class="container-fluid header_gradient">
         </div> -->
    </header>
     <section id="body">
        <div class="container-fluid all-page ">
            <div class="container-fluid main-body-top" style="min-height: 426px;">
                <div class="container-fluid header1">
                    <div class="container header2">
                        <div class="row">
                            <div class="col col-lg-8">
                                
                                <div class="col col-lg-12 text-right">
                                    <h4 class="title_">درمانگاه های تخصصی و فوق تخصصی بیمارستان دکتر شریعتی</h4>
                                </div>
                            </div>
                            <div class="col col-lg-4 text-left">
                                <a href="index.php" style="margin-top: 18px" class="btn btn-white ">صفحه  نخست</a>
                                <a href="#" style="margin-top: 18px" class="btn btn-white back">صفحه قبل</a>
                            </div>
                        </div>
                    </div>
                </div>
                  



    <ul class="breadcrumb">
 <li class="active "><a href="#">استعلام نوبت   </a> </li>                                       </ul>




<div class="container-fluid main-body_" style="min-height: 426px;">
    <div class="container content text-center">

        <div class="seprator">
            <span class="glyphicon glyphicon-star sep-glyp1"></span>
            <span class="glyphicon glyphicon-star sep-glyp2"></span>
            <span class="glyphicon glyphicon-star sep-glyp3"></span>
            <h3>استعلام نوبت</h3>
            <div class="course-div-sep1">
                <div class="course-div-sep2"></div>
            </div>
        </div>
        <div class="row">

            
        <form action="" method="post"> <div class="row">
    <div class="col-lg-12 text-right">

     <div class="form-group form-group-black wow bounceInRight animated">
       <div class="alert alert-dismissable alert-info">
         با وارد کردن کدملی، می توانید از نوبت های ثبت شده خود اطلاع پیدا کنید.
      </div>
     </div>

     
     <?php 
    if(isset($_SESSION['result_false'])):

    ?>
<div class="alert alert-dismissable alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>توجه ! </strong>شما نوبتی رزور نکرده اید.
    </div>
    <?php
    unset($_SESSION['result_false']);
   endif;

     ?>

     <div class="col-lg-6">
      <div class="col-lg-6">
       <div class="form-group form-group-black">
        <label class="control-label" for="inputLarge">کد ملی : <span class="text-danger">*</span></label>
        <input class="form-control force onlynumber"  maxlength="10" name="code" placeholder="کد ملی را در این قسمت وارد کنید" type="text" value="" required="required" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('کد ملی خود را وارد کنید')">
        <span class="material-input"></span>
       </div>
      </div>
        <div style="margin-top:23px;" class="form-group text-left">
        <button name="btn" type="submit" id="btn-turnlist-search" data-id="print-turn-list" class="sub_validation btn btn-info  btn-lg">جستجو </button>
       </div>
      

     </div>
     <?php
      if(@mysqli_num_rows($result) <= 0):
        
    ?>
    
 
     
       <?php 
       else:
       ?>
       <table class="table table-striped table-hover">
           <thead>
               <tr>
                   <td>نام :</td>
                   <td>نام خانوادگی :</td>
                   <td>کد ملی :‌</td>
                   <td>بیمارستان</td>
                   <td>نام دکتر :</td>
               </tr>
           </thead>
           <tbody>
           <?php 
                while($row = mysqli_fetch_assoc($result)):
                $res = show_child_by_id($row['hospital_id']);
                $hospital = mysqli_fetch_assoc($res);

                $res_doctor = show_doctor_name($row['doctor_id']);
                $doctor = mysqli_fetch_assoc($res_doctor);
                
                
            ?>
               <tr>
                   <td><?php echo $row['name']; ?></td>
                   <td><?php echo $row['lastname']; ?></td>
                   <td><?php echo $row['national_code']; ?></td>
                   <td><?php echo $hospital['name']; ?></td>
                   <td><?php echo $doctor['name']." ".$doctor['lastname']; ?></td>
               </tr>
            <?php 
                endwhile;
            ?>
           </tbody>
            
            
       </table>
       <?php
       endif;
    
       ?>
    </div>
   </div>
</form>


        
    </div>
</div>
            <div class="container-fluid footer">
                <div class="container">
                    <h4 class="company">
                        
                    </h4>
                    <p>ساخته شده توسط : سحر تیموری</p>
                </div>
            </div>
        </div>

    </section>
    <!-- <script src="assets/js/jquery.js"></script> -->

    <!-- <script src="assets/js/bootstrap.js"></script> -->

    
    <script>
        var WindowHeight = $(document).height() - 1;
        var HeaderHeight = $('.header0').innerHeight() + $('.header_gradient').innerHeight() + $('.header1').innerHeight();
        var footerHeight = $('.footer').innerHeight();
        var bodyHeight = WindowHeight - HeaderHeight - footerHeight;
        $('.main-body_').css('min-height', bodyHeight);
        $('.main-body-top').css('min-height', bodyHeight);
        $('.back').click(function () {
            history.back();
            return false;
           // history.go(-1)
        })
    </script>



</body></html>